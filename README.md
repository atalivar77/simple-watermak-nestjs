# Simple Watermark - NestJS

This is a project built with NestJS to add watermarks to images. The idea is to showcase a Node.js project using the following technologies and libraries:

* MongoDB (for persistence)
* MinIO (for the image bucket)
* Libraries like Sharp, Jimp, Multer (for image handling), and common ones like Mongoose or Swagger, among others.

## Requirements to Run the Project

- Node.js
- Docker (for running MongoDB and MinIO containers or have them installed previously)

Here is the link to my other repository with Docker images:
[https://gitlab.com/atalivar77/docker-containers-templates](https://gitlab.com/atalivar77/docker-containers-templates)

## Steps to Run the Project Locally

1. Clone the repository locally.
2. Configure the `.env` file (use the `.env.example` as a template and rename it to `.env`).
3. Execute:
   ```bash
   npm install
4. Run:
   ```bash
   npm run start

## Screenshots:

### Examples:

![Imagen](https://gitlab.com/atalivar77/simple-watermak-nestjs/-/raw/main/public/5a3d94b2-02d3-49d5-9025-d310a8d24e0f.jpg?ref_type=heads)

![Imagen](https://gitlab.com/atalivar77/simple-watermak-nestjs/-/raw/main/public/434996e3-fc6d-414a-a254-cb57058090c8.jpg?ref_type=heads)

### MinIO local bucket
![Imagen](https://gitlab.com/atalivar77/simple-watermak-nestjs/-/raw/main/public/minio.dashboard.png?ref_type=heads)

### MongoDB local data
![Imagen](https://gitlab.com/atalivar77/simple-watermak-nestjs/-/raw/main/public/mongo.data.png?ref_type=heads)


