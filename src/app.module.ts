import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ImageModule } from './image/image.module';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigModule } from '@nestjs/config';
import config from './config/configuration';
import { HealthModule } from './health/health.module';
import { join } from 'path';
import 'dotenv/config';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      serveRoot: '/static/',
    }),
    MongooseModule.forRoot(
      `${process.env.MONGO_DB_URL}${process.env.MONGO_DB_DATABASE}`,
    ),
    ConfigModule.forRoot({
      load: [config],
    }),
    HealthModule,
    ImageModule,
    MulterModule.register({
      dest: './images',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
