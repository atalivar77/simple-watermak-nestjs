export type ResHealth = {
  status: number;
  msg: string;
};

export class QueryParamsDto {
  id: number;
}
