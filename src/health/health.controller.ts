import { Controller, Get } from '@nestjs/common';
import { HealthService } from './health.service';
import { ResHealth } from './health.dto';

@Controller('health')
export class HealthController {
  constructor(private readonly healthService: HealthService) {}

  @Get()
  health(): ResHealth {
    return this.healthService.getHealth();
  }
}
