import { Injectable } from '@nestjs/common';
import { ResHealth } from './health.dto';

@Injectable()
export class HealthService {
  getHealth(): ResHealth {
    return { status: 200, msg: 'Server starter correctly' };
  }
}
