import { Injectable } from '@nestjs/common';
import * as Minio from 'minio';
import { Model } from 'mongoose';
import { Config } from '../config/config.service';
import * as Jimp from 'jimp';
import * as sharp from 'sharp';
import * as fs from 'fs';
import * as path from 'path';
import { extname } from 'path';
import { Image } from './schema/image.schema';
import { v4 as uuidv4 } from 'uuid';
import { InjectModel } from '@nestjs/mongoose';
import { responseData, responseMessage } from './image.comunication';

@Injectable()
export class ImageService {
  private readonly minioClient: Minio.Client;

  constructor(
    private readonly configService: Config,
    @InjectModel(Image.name) private readonly imageModel: Model<Image>,
  ) {
    //Client Minio's bucket
    this.minioClient = new Minio.Client(this.configService.getS3Config());
  }

  async checkConnection() {
    try {
      // Attempt to perform an operation that requires a connection
      const bucketName = this.configService.getS3Config().bucket;
      await this.minioClient.bucketExists(bucketName); // Change this to your bucket name
      return true;
    } catch (error) {
      // If there is an error, the connection is not established
      console.error('Error connecting to MinIO:', error.message);
      return false;
    }
  }

  async create(
    image: Express.Multer.File,
  ): Promise<responseMessage | responseData<Image>> {
    try {
      const bucketName = this.configService.getS3Config().bucket;
      const waterMarkImage = this.configService.getWMarkConfig().image;
      // Convert the AVIF image to PNG using sharp
      const convertedImageBuffer = await sharp(image.buffer).png().toBuffer();
      const watermarksRelativeDirectory = '../watermarks';
      // Full path of the watermark file
      const watermarkPath = path.join(
        process.cwd(),
        'src',
        watermarksRelativeDirectory,
        waterMarkImage,
      );
      // Read the watermark from the file
      const watermarkBuffer = fs.readFileSync(watermarkPath);

      // Load the converted image using Jimp
      const jimpImage = await Jimp.read(convertedImageBuffer);

      // Load the watermark from the buffer
      const waterMark = await Jimp.read(watermarkBuffer);

      jimpImage.composite(waterMark, 10, 10, {
        mode: Jimp.BLEND_SOURCE_OVER,
        opacitySource: this.configService.getWMarkConfig().opacitySource,
        opacityDest: this.configService.getWMarkConfig().opacityDest,
      });

      const imageBuffer = await jimpImage.getBufferAsync(Jimp.MIME_PNG);
      const id = uuidv4();
      const newName = `${id}${extname(image.originalname)}`;

      await this.minioClient.putObject(
        bucketName,
        newName,
        imageBuffer,
        image.size,
      );
      const imageUrl = `http://${this.configService.getS3Config().endPoint}:${this.configService.getS3Config().port}/${bucketName}/${newName}`;

      return {
        data: await this.imageModel.create({
          uuid: id,
          url: imageUrl,
          newName: newName,
          originalName: image.originalname,
        }),
        status: 200,
      };
    } catch (error) {
      return { message: error.message, status: 500 };
    }
  }

  async findAll(): Promise<responseMessage | responseData<Image[]>> {
    try {
      const images = await this.imageModel.find({ active: true });
      return { data: images, status: 200 };
    } catch (error) {
      return { message: error.message, status: 500 };
    }
  }

  async findOne(id: string): Promise<responseMessage | responseData<Image>> {
    try {
      const image = await this.imageModel.findOne({ uuid: id, active: true });
      return image
        ? { data: image, status: 200 }
        : { message: 'Image not found', status: 404 };
    } catch (error) {
      return { message: error.message, status: 500 };
    }
  }

  async remove(id: string): Promise<responseMessage> {
    try {
      const image = await this.imageModel.findOne({ uuid: id });
      if (!image) {
        return { message: 'Image not found', status: 404 };
      }
      image.active = false;
      await image.save();
      //delete from bucket
      const bucketName = this.configService.getS3Config().bucket;
      await this.minioClient.removeObject(bucketName, image.newName);
      return { message: 'Image deleted successfully', status: 200 };
    } catch (error) {
      return { message: error.message, status: 500 };
    }
  }
}
