import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type ImageDocument = HydratedDocument<Image>;

@Schema()
export class Image {
  @Prop({ required: true })
  uuid: string;
  @Prop({ required: true })
  url: string;
  @Prop({ required: true })
  newName: string;
  @Prop({ required: true })
  originalName: string;
  @Prop({ default: true })
  active: boolean;
}

export const ImageSchema = SchemaFactory.createForClass(Image);
