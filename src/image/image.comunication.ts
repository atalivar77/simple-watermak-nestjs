export type responseData<T> = {
  data: T;
  status: number;
};

export type responseMessage = {
  message: string;
  status: number;
};
