import {
  Controller,
  Get,
  Post,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ImageService } from './image.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('products')
@Controller('image')
export class ImageController {
  constructor(private readonly imageService: ImageService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  @UsePipes(
    new ValidationPipe({
      transform: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  )
  async create(@UploadedFile() file: Express.Multer.File) {
    if (!file) {
      return { message: 'File cannot be empty', status: 400 };
    }
    if ((await this.imageService.checkConnection()) === false)
      return { message: 'The bucket is not available.', status: 404 };
    const ext = file.originalname.split('.').pop().toLowerCase();
    if (!ext || (ext !== 'jpg' && ext !== 'png')) {
      return { message: 'Invalid file format', status: 400 };
    }
    return await this.imageService.create(file);
  }

  @Get()
  async findAll() {
    return await this.imageService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.imageService.findOne(id);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.imageService.remove(id);
  }
}
