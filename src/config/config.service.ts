// s3-config.service.ts
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class Config {
  constructor(private readonly configService: ConfigService) {}
  getS3Config() {
    return {
      endPoint: this.configService.get<string>('s3.endPoint'),
      port: this.configService.get<number>('s3.port'),
      useSSL: this.configService.get<boolean>('s3.useSSL'),
      accessKey: this.configService.get<string>('s3.accessKey'),
      secretKey: this.configService.get<string>('s3.secretKey'),
      bucket: this.configService.get<string>('s3.bucket'),
    };
  }
  getWMarkConfig() {
    return {
      image: this.configService.get<string>('watermark.image'),
      opacitySource: this.configService.get<number>('watermark.opacitySource'),
      opacityDest: this.configService.get<number>('watermark.opacityDest'),
    };
  }
}
