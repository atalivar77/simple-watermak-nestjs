export default () => ({
  mongoDB: {
    url: process.env.MONGO_DB_URL || 'mongodb://localhost:27017',
    database: process.env.MONGO_DB_DATABASE || '/watermark',
  },
  s3: {
    endPoint: process.env.S3_BASE_URL || 'localhost',
    port: parseInt(process.env.S3_PORT, 10) || 9000,
    useSSL: process.env.S3_USE_SSL === 'true' || false,
    accessKey: process.env.S3_ACCESS_KEY || 'my_access_key_1',
    secretKey: process.env.S3_SECRET_KEY || 'my_secret_access_key_1',
    bucket: process.env.S3_BUCKET || 'images',
  },
  watermark: {
    image: process.env.WM_IMAGE || 'example.png',
    opacitySource: parseFloat(process.env.WM_OPACITY_SOURCE) || 0.5,
    opacityDest: parseFloat(process.env.WM_OPACITY_DEST) || 0.5,
  },
});
